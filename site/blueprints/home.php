<?php if(!defined('KIRBY')) exit ?>

title: Home
pages: tiles
fields:
  title:
    label: Title
    type:  text
  head_title:
    label: Head Title
    type: textarea
  head_text:
    label: Text
    type:  textarea
    size:  large