<?php if(!defined('KIRBY')) exit ?>

username: admin
password: >
  $2a$10$tgl8MoTZXUa2kKeIRpmVret4/0NsGm5cKk6Ov4O88fLC0G5O1QoiS
email: rikapm5@gmail.com
language: en
role: admin
history:
  - elements
  - home/veroeros
  - home/aliquam
  - home/tempus
  - home/magna
